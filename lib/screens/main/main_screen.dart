import 'package:flutter/material.dart';
import 'package:responsive_email_ui/components/side_menu.dart';
import 'package:responsive_email_ui/responsive.dart';
import 'package:responsive_email_ui/screens/email/email_screen.dart';

import 'components/list_of_emails.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
          body: ResponsiveWidget(
        mobile: ListOfEmails(),
        tablet: Row(
          children: [
            Expanded(flex: 6, child: ListOfEmails()),
            Expanded(flex: 9, child: EmailScreen())
          ],
        ),
        desktop: Row(
          children: [
            Expanded(
              flex: _size.width > 1340 ? 2 : 4,
              child: SideMenu(),
            ),
            Expanded(
              flex: _size.width > 1340 ? 3 : 5,
              child: ListOfEmails(),
            ),
            Expanded(
              flex: _size.width > 1340 ? 8 : 10,
              child: EmailScreen(),
            ),
          ],
        ),
      )),
    );
  }
}
