import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_email_ui/responsive.dart';

import '../../../constants.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(kDefaultPadding),
      child: Row(
        children: [
          if (ResponsiveWidget.isMobile(context)) BackButton(),
          IconButton(
            icon: SvgPicture.asset(
              // WebsafeSvg.asset(
              "assets/icons/Trash.svg",
              width: 24,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: SvgPicture.asset(
              // WebsafeSvg.asset(
              "assets/icons/Reply.svg",
              width: 24,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: SvgPicture.asset(
              // WebsafeSvg.asset(
              "assets/icons/Reply all.svg",
              width: 24,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: SvgPicture.asset(
              // WebsafeSvg.asset(
              "assets/icons/Transfer.svg",
              width: 24,
            ),
            onPressed: () {},
          ),
          Spacer(),
          if (ResponsiveWidget.isDesktop(context))
            IconButton(
              icon: SvgPicture.asset(
                // WebsafeSvg.asset(
                "assets/icons/Printer.svg",
                width: 24,
              ),
              onPressed: () {},
            ),
          IconButton(
            icon: SvgPicture.asset(
              // WebsafeSvg.asset(
              "assets/Icons/Markup.svg",
              width: 24,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: SvgPicture.asset(
              // WebsafeSvg.asset(
              "assets/icons/More vertical.svg",
              width: 24,
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
